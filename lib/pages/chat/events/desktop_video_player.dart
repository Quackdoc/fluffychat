//from other project
import 'dart:async';
import 'dart:io';
import 'dart:math';

//base
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//mediakit
import 'package:media_kit/media_kit.dart';
import 'package:media_kit_video/media_kit_video.dart';

import 'package:path/path.dart' as path;
//fluffychat
import 'package:fluffychat/pages/chat/events/image_bubble.dart';
import 'package:fluffychat/utils/localized_exception_extension.dart';
import 'package:fluffychat/utils/matrix_sdk_extensions/event_extension.dart';

//TEMP
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:path_provider/path_provider.dart';

class EventDesktopPlayer extends StatefulWidget {
  final Event event;
  const EventDesktopPlayer(this.event, {Key? key}) : super(key: key);

  @override
  EventDesktopPlayerState createState() => EventDesktopPlayerState();
}

//begin seekbar
class SeekBar extends StatefulWidget {
  final Player player;
  const SeekBar({
    Key? key,
    required this.player,
  }) : super(key: key);

  @override
  State<SeekBar> createState() => _SeekBarState();
}

class _SeekBarState extends State<SeekBar> {
  bool isPlaying = false;
  Duration position = Duration.zero;
  Duration duration = Duration.zero;
  double volume = 0.5;

  List<StreamSubscription> subscriptions = [];

  @override
  void initState() {
    super.initState();
    isPlaying = widget.player.state.isPlaying;
    position = widget.player.state.position;
    duration = widget.player.state.duration;
    volume = widget.player.state.volume;
    
    subscriptions.addAll(
      [
        widget.player.streams.isPlaying.listen((event) {
          setState(() {
            isPlaying = event;
          });
        }),
        widget.player.streams.position.listen((event) {
          setState(() {
            position = event;
          });
        }),
        widget.player.streams.duration.listen((event) {
          setState(() {
            duration = event;
          });
        }),
        widget.player.streams.volume.listen((event) {
          setState(() {
            volume = event;
          });
        }),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    for (final s in subscriptions) {
      s.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
            onPressed: widget.player.playOrPause,
            icon: Icon(
              isPlaying ? Icons.pause : Icons.play_arrow,
            ),
            color: Theme.of(context).toggleableActiveColor,
            iconSize: 36.0,
          ),
          Text(position.toString().substring(2, 7)),
          Expanded(
            child: Slider(
              min: 0.0,
              max: duration.inMilliseconds.toDouble(),
              value: position.inMilliseconds.toDouble().clamp(
                    0,
                    duration.inMilliseconds.toDouble(),
                  ),
              onChanged: (e) {
                setState(() {
                  position = Duration(milliseconds: e ~/ 1);
                });
              },
              onChangeEnd: (e) {
                widget.player.seek(Duration(milliseconds: e ~/ 1));
              },
            ),
          ),
          //Text(duration.toString().substring(2, 7)),
          //IconButton(
          //  onPressed: ,
          //  icon: Icon(
          //    volume = 0.0 ? Icons.volume_off : Icons.volume_up,
          //  ),
          //  color: Theme.of(context).primaryColor,
          //  iconSize: 36.0,
          //),
        ],
      )
      
    );
  }
}
//end seekbar
class EventDesktopPlayerState extends State<EventDesktopPlayer> {

      // Create a [Player] instance from `package:media_kit`.
  final Player player = Player();
  // Reference to the [VideoController] instance from `package:media_kit_core_video`.
  VideoController? controller;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller = await VideoController.create(player.handle);
      setState(() {});
    });
  }

  bool _isDownloading = false;
  bool Triggered = false;
  bool isVisible = false;
  String? _networkUri;
  File? _tmpFile;

    void _downloadAction() async {
    setState(() => _isDownloading = true);
    setState(() => Triggered = false);
    try {
      final videoFile = await widget.event.downloadAndDecryptAttachment();

      final tempDir = await getTemporaryDirectory();
      final fileName = Uri.encodeComponent(
          widget.event.attachmentOrThumbnailMxcUrl()!.pathSegments.last);
      final file = File('${tempDir.path}/${fileName}_${videoFile.name}');
      if (await file.exists() == false) {
        await file.writeAsBytes(videoFile.bytes);
      }
      _tmpFile = file;

      final tmpFile = _tmpFile;

        await player.open(Playlist([
            Media(tmpFile!.path)
        ]));
    } on MatrixConnectionException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(e.toLocalizedString(context)),
      ));
    } catch (e, s) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(e.toLocalizedString(context)),
      ));
      Logs().w('Error while playing video', e, s);
    } finally {
      setState(() => _isDownloading = false);
      setState(() => Triggered = true);
    }
  }

    @override
  void dispose() {
    Future.microtask(() async {
      await controller?.dispose();
      await player.dispose();
    });;
    super.dispose();
  }

  static const String fallbackBlurHash = 'L5H2EC=PM+yV0g-mq.wG9c010J}I';

    @override
  Widget build(BuildContext context) {
    final hasThumbnail = widget.event.hasThumbnail;
    final blurHash = (widget.event.infoMap as Map<String, dynamic>)
            .tryGet<String>('xyz.amorgan.blurhash') ??
        fallbackBlurHash;


    return Material(
      color: Color.fromARGB(0, 0, 0, 0),
      child: AspectRatio(
        //TODO: make aspectratio customizable, figure out ratio from mpv needs set to proper align with side.
        aspectRatio: 16/9,
        child: Triggered == true
            ? Stack(
              //alignment: Alignment.bottomCenter,
              children: [
                  Center(child: Video(controller: controller)),
                  if(isVisible)
                  Container(
                    child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      color: Color.fromARGB(125, 0, 0, 0),
                      child: SeekBar(player: player),
                    )
                  )
                ),
                MouseRegion(
                  onEnter: (PointerEvent details)=>setState(()=>isVisible = true),
                  onExit: (PointerEvent details)=>setState(()=>isVisible = false),
                  opaque: false,
                )
              ]
            )
            : Stack(
                children: [
                  if (hasThumbnail)
                    Center(
                      child: ImageBubble(
                        widget.event,
                        tapToView: false,
                      ),
                    )
                  else
                    BlurHash(hash: blurHash),
                  Center(
                    child: OutlinedButton.icon(
                      style: OutlinedButton.styleFrom(
                        backgroundColor: Theme.of(context).colorScheme.surface,
                      ),
                      icon: _isDownloading
                          ? const SizedBox(
                              width: 24,
                              height: 24,
                              child: CircularProgressIndicator.adaptive(
                                  strokeWidth: 2),
                            )
                          : const Icon(Icons.download_outlined),
                      label: Text(
                        _isDownloading
                            ? L10n.of(context)!.loadingPleaseWait
                            : L10n.of(context)!.videoWithSize(
                                widget.event.sizeString ?? '?MB'),
                      ),
                      onPressed: _isDownloading ? null : _downloadAction,
                    ),
                  ),
                ],
              ),
      ),
    );
  }

}